package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.MainActivity;
import com.example.administrator.test.View.MenuGeneral;

import retrofit2.Call;
import retrofit2.Callback;

public class UpdatePasswordCustomerBFragment extends Fragment {
    Button btnUpdatePassword;
    ProgressDialog progress;
    Client client;
    EditText EdtTxt_NewPassword, EdtTxt_OldPassword, EdtTxt_Login;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_update_info_password_customer, container, false);
        initializeData();
        initializeViews(view);
        makeProgresBar();
        actionButton();
        return view;
    }

    private void initializeData() {
        client = ClientController.getClient();
    }

    private void initializeViews(View view) {
        btnUpdatePassword = view.findViewById(R.id.btnUpdateInformationPassword);
        EdtTxt_NewPassword = view.findViewById(R.id.EdtTxt_NewPassword);
        EdtTxt_OldPassword = view.findViewById(R.id.EdtTxt_OldPassword);
        EdtTxt_Login = view.findViewById(R.id.EdtTxt_Login);
        EdtTxt_Login.setText(client.getLogin());
        EdtTxt_Login.setEnabled(false);
    }

    public void actionButton() {
        btnUpdatePassword.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showProgresBar();
                ServiceClient.instance().updatePasswordClient(client.getId(), client.getLogin(), EdtTxt_OldPassword.getText().toString(), EdtTxt_NewPassword.getText().toString()).enqueue(new Callback<Client>() {
                    @Override
                    public void onResponse(Call<Client> call, retrofit2.Response<Client> response) {
                        dismissProgresBar();
                        if (response.code() == 200) {
                            ClientController.destroyClient();
                            Intent myIntent = new Intent(getContext(), MainActivity.class);
                            Toast.makeText(getContext(), "success updating", Toast.LENGTH_LONG).show();
                            getContext().startActivity(myIntent);
                        } else
                            Toast.makeText(getContext(), "ERREUR" + response.code(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<Client> call, Throwable t) {
                        Toast.makeText(getContext(), "Failure", Toast.LENGTH_LONG).show();
                        dismissProgresBar();
                    }
                });
            }
        });
    }

    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }
}
