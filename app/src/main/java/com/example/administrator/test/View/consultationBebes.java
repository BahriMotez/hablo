package com.example.administrator.test.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.R;

public class consultationBebes extends AppCompatActivity {

    TextView text;
    Childs childs;
    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_bebes);

        childs = (Childs) getIntent().getSerializableExtra("childs");

        ListView listeView = (ListView) findViewById(R.id.listeView);
        CustomAdapter adapter = new CustomAdapter(this);

        listeView.setAdapter(adapter);
    }

    class CustomAdapter extends BaseAdapter
    {
        consultationBebes consultationBebes;
        public CustomAdapter(consultationBebes consultationBebes)
        {
            this.consultationBebes = consultationBebes;
        }
        @Override
        public int getCount() {
            return childs.getChilds().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custom_layout,null);
//            ImageView imageView_imageBebe = (ImageView)view.findViewById(R.id.imageView_imageBebe);
//            TextView textView_nomPrenom = (TextView) view.findViewById(R.id.textView_nomprenom);
//            TextView textView_status = (TextView) view.findViewById(R.id.textView_status);
//
//            Picasso.with(consultationBebes).load(childs.getChilds().get(i).getSrcImg()).into(imageView_imageBebe);
//            textView_nomPrenom.setText(childs.getChilds().get(i).getName()+" "+childs.getChilds().get(i).getLastname());
//            textView_nomPrenom.setText(childs.getChilds().get(i).getName());
            return view;
        }
    }
}
