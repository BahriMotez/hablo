package com.example.administrator.test.Entites;

public class Device {
    private String id;
    private String name;
    private String active;
    private String inscriDate;
    private String token;
    private String customer_id;

    public Device(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getInscriDate() {
        return inscriDate;
    }

    public void setInscriDate(String inscriDate) {
        this.inscriDate = inscriDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }
}
