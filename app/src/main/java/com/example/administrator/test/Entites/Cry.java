package com.example.administrator.test.Entites;

import java.io.Serializable;

public class Cry implements Serializable {
    private Integer id;
    private Integer idAlert;
    private String srcAudioFile;

    public Cry(Integer id, Integer idAlert, String srcAudioFile) {
        this.id = id;
        this.idAlert = idAlert;
        this.srcAudioFile = srcAudioFile;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAlert() {
        return idAlert;
    }

    public void setIdAlert(Integer idAlert) {
        this.idAlert = idAlert;
    }

    public String getSrcAudioFile() {
        return srcAudioFile;
    }

    public void setSrcAudioFile(String srcAudioFile) {
        this.srcAudioFile = srcAudioFile;
    }

    @Override
    public String toString() {
        return "Cry{" +
                "id=" + id +
                ", idAlert=" + idAlert +
                ", srcAudioFile='" + srcAudioFile + '\'' +
                '}';
    }
}
