package com.example.administrator.test.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.test.Entites.Alert;
import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ItemClickListener;
import com.example.administrator.test.View.AlertConsultationActivity;
import com.example.administrator.test.View.MainActivity;
import com.example.administrator.test.View.MenuGeneral;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AlertsRecyclerViewAdapter extends RecyclerView.Adapter<AlertsRecyclerViewAdapter.ListItemViewHolder> {
    Context ctx;
    private ArrayList<Alert> items;
    private SparseBooleanArray selectedItems;

    public AlertsRecyclerViewAdapter(Context ctx, ArrayList<Alert> AlertsData) {
        if (AlertsData == null) {
            throw new IllegalArgumentException("Alerts Data List must not be null");
        }
        items = AlertsData;
        selectedItems = new SparseBooleanArray();
        this.ctx = ctx;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.custom_layout_show_alerts_home, viewGroup, false);
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        final Alert alert = items.get(position);

        viewHolder.alertDate.setText(CalculateDate(alert.getAlertDate()));
        viewHolder.description.setText(String.valueOf(alert.getTitle()));
        switch (alert.getTitle()) {
            case "Hungry Baby": {
                viewHolder.img_notif_custum.setBackgroundResource(R.drawable.faimicon);
                break;
            }
            case "Moody Baby": {
                viewHolder.img_notif_custum.setBackgroundResource(R.drawable.moodicon);
                break;
            }
            case "Sleepy Baby": {
                viewHolder.img_notif_custum.setBackgroundResource(R.drawable.sleepicon);
                break;
            }
            case "Hygiene Baby": {
                viewHolder.img_notif_custum.setBackgroundResource(R.drawable.hygieneicon);
                break;
            }
        }
        viewHolder.itemView.setActivated(selectedItems.get(position, false));

        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent intent = new Intent(ctx, AlertConsultationActivity.class);

                intent.putExtra("clickAction", "yes");
                intent.putExtra("idalert", alert.getId().toString());
                intent.putExtra("title", alert.getTitle());
                intent.putExtra("message", alert.getDescription());

                ctx.startActivity(intent);
            }
        });

    }

    private String CalculateDate(String alertDate) {
        int minutesAlert, heuresAlert, minutesDateSystem, heuresDateSystem;
        minutesAlert = Integer.parseInt(alertDate.substring(20, 22));
        heuresAlert = Integer.parseInt(alertDate.substring(17, 19));
        Date currentTime = Calendar.getInstance().getTime();
        minutesDateSystem = currentTime.getMinutes();
        heuresDateSystem = currentTime.getHours();
        int datedate =currentTime.getDate();
        int datedatef =Integer.parseInt(alertDate.substring(5, 7));
        if (currentTime.getDate()!=Integer.parseInt(alertDate.substring(5, 7)))
            return alertDate.substring(5,16);
        else if(heuresDateSystem-heuresAlert>0 && heuresDateSystem-heuresAlert<24)
            return (""+(heuresDateSystem-heuresAlert)+" hours ago " );
        else if (heuresDateSystem-heuresAlert<=0)
            return (""+(minutesDateSystem-minutesAlert)+" minutes ago ");
        return alertDate.substring(5,16);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public final static class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView alertDate;
        TextView description;
        ImageView img_notif_custum;
        private ItemClickListener itemClickListener;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            alertDate = itemView.findViewById(R.id.textView_alertDate);
            description = itemView.findViewById(R.id.textView_descriptionAlerts);
            img_notif_custum = itemView.findViewById(R.id.img_notif_custum);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }
}
