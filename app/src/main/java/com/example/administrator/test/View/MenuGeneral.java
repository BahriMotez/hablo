package com.example.administrator.test.View;

import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Fragments.ClassificationNotificationBFragment;
import com.example.administrator.test.Fragments.HomeBFragment;
import com.example.administrator.test.Fragments.NotificationBFragment;
import com.example.administrator.test.Fragments.SettingsDefaultFragment;
import com.example.administrator.test.R;

public class MenuGeneral extends AppCompatActivity {
    Client client;
    Intent mServiceIntent;
    BottomNavigationView bottomNav;
    Context ctx;
    String idAlert;
    public static final String CHANNEL_ID = "hablo_channel1";
    private NotificationManagerCompat notificationMnager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.activity_menu_general);
        notificationMnager = NotificationManagerCompat.from(this);
        //get the Client from the sharedpreference
        client = ClientController.getClient();



        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);


        setDefaultFragment();

        //set the fragment if the client pressend on the notification

    }

    public void setHeaderText(String text){
        TextView textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(text);
    }

    @Override
    protected void onDestroy() {
        if(mServiceIntent != null)
            stopService(mServiceIntent);
        super.onDestroy();
    }

    public Context getCtx() {
        return ctx;
    }

    private void setDefaultFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeBFragment()).addToBackStack("home").commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.settingsFrag:
                            selectedFragment = new SettingsDefaultFragment();
                            break;
                        case R.id.notifFrag:
                            selectedFragment = new ClassificationNotificationBFragment();
                            break;
                        case R.id.homeFragment:
                            selectedFragment = new HomeBFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).addToBackStack("home").commit();

                    return true;
                }
            };
}

