package com.example.administrator.test.Services;



import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Entites.Child;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ServiceBebe {
    @Multipart
    @POST("Save")
    Call<Child> createAccount(@Part("birthDate") RequestBody birthDate,
                              @Part("gender") RequestBody gender
                              );


    @GET("bebe")
    Call<Childs> consulterBebe();

}
