package com.example.administrator.test.RetrofitPkg;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Services.ServiceApi;
import com.example.administrator.test.Utils.MyApplication;
import com.example.administrator.test.View.MenuGeneral;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientResponse {
    Context ctx;
    String url;
    public Client c;
    MyApplication myApp;
    public Client getC() {
        return c;
    }

    public void setC(Client c) {
        this.c = c;
    }

    public ClientResponse(Context ctx, String url) {
        this.ctx = ctx;
        this.url = url;
    }

    public Client reponse(Client clt)
    {Client c = new Client(clt) ;
        return c ;
    }
   public void  sendNetworkRequest(Client clt ) {

       Retrofit.Builder builder = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create());
       Retrofit retrofit = builder.build();
       final ServiceApi client = retrofit.create(ServiceApi.class);
       Call<Client> call = client.login(clt.getLogin(), clt.getPwd());

       call.enqueue(new Callback<Client>() {

           @Override
           public void onResponse(Call<Client> call, retrofit2.Response<Client> response) {
               Toast.makeText(ctx, "yes" + response.code(), Toast.LENGTH_LONG).show();

               if (response.code() == 200)
               {

                   Client clt = response.body();
                   Toast.makeText(ctx, "yes" + response.body(), Toast.LENGTH_LONG).show();
                   Intent myIntent = new Intent(ctx, MenuGeneral.class);
//                 myIntent.putExtra("client", response.body());
                   ClientController.setClient(clt);
                   ctx.startActivity(myIntent);

               }
               else
                   Toast.makeText(ctx, " Verify your login or pwd" + response.code(), Toast.LENGTH_LONG).show();
           }

           @Override
           public void onFailure(Call<Client> call, Throwable t) {
               Toast.makeText(ctx, "Failure", Toast.LENGTH_LONG).show();
               //  Log.d("Login", t.getLocalizedMessage());
              /*  Intent myIntent = new Intent(MainActivity.this,MenuGeneral.class);
                startActivity(myIntent);*/
           }
       });


   }}

