package com.example.administrator.test.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText pwd;
    ImageButton btn;
    String loginEditTextValue,pwdEditTextValue;
    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
        makeProgresBar();
        //test if the client save his login
        if(ClientController.getClient() != null)
        {
            //Open our main we know who is the client
            openMenuActivity();
        }
        //make the login
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loginEditTextValue = login.getText().toString();
                pwdEditTextValue = pwd.getText().toString();
                ButtonAction();
            }

        });
    }

    private void openMenuActivity() {
        Intent myIntent = new Intent(MainActivity.this, MenuGeneral.class);
        MainActivity.this.startActivity(myIntent);
        finish();
    }

    private void ButtonAction() {
        showProgresBar();
        ServiceClient.instance().login(loginEditTextValue,pwdEditTextValue).enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if (response.code() == 200) {
                    dismissProgresBar();
                    Client clt = response.body();
                    Intent myIntent = new Intent(MainActivity.this, MenuGeneral.class);
                    ClientController.setClient(clt);
                    //get the token device
                    setPhoneToken();
                    MainActivity.this.startActivity(myIntent);
                } else {
                    dismissProgresBar();
                    Toast.makeText(MainActivity.this, "UNAUTHORIZED Verify Login/Pass", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                // start loading dismiss
                dismissProgresBar();
            }
        });
    }

    private void setPhoneToken() {
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            //String deviceToken = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            ServiceClient.instance().setDeviceToken(ClientController.getClient().getId(),refreshedToken).enqueue(new Callback<Client>() {
                @Override
                public void onResponse(Call<Client> call, Response<Client> response) {
                    if (response.code() == 200) {
                        Client clt = response.body();
                        Log.i("OKK","evreything wrok just fine");
                    } else {
                        Log.d("something","Something went wrong here");
                    }
                }

                @Override
                public void onFailure(Call<Client> call, Throwable t) {
                    Log.e("Error","something wrong"+t.getLocalizedMessage());
                }
            });
        }catch (Exception e) {
            Log.d("TAG", "Failed to complete token refresh", e);
        }
    }

    private void makeProgresBar(){
        progress = new ProgressDialog(MainActivity.this);
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar(){progress.dismiss();}

    private void showProgresBar(){progress.show();}

    private void initializeViews() {
        btn = findViewById(R.id.button);
        login = findViewById(R.id.editText2);
        pwd = findViewById(R.id.editText);
    }
}
