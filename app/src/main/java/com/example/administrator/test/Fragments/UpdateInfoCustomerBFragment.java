package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.MenuGeneral;

import retrofit2.Call;
import retrofit2.Callback;

public class UpdateInfoCustomerBFragment extends Fragment {
    View view;
    Client client;
    ProgressDialog progress;
    EditText EdtTxt_Name,EdtTxt_LastName,EdtTxt_Email,EdtTxt_Phone;
    Button btnUpdateProfile;
    ImageView imgCustomer_accountFragUpdate;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_update_info_customer, container, false);
        //create the object our progress bar
        makeProgresBar();
        //get the connected client from the Client Controller (SharedPreference)
        initializeData();
        //get the buttons from the view
        initializeViews(view);
        //setThe client information into the activity
        setClientInformation();

        //setButton Actions
        updateInformationCutomer();
        return view;
    }

    private void updateInformationCutomer() {
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showProgresBar();
                ServiceClient.instance().updateClient(client.getId(),
                                                      EdtTxt_Name.getText().toString(),
                                                      EdtTxt_LastName.getText().toString(),
                                                      EdtTxt_Email.getText().toString(),
                                                      EdtTxt_Phone.getText().toString()).enqueue(new Callback<Client>() {
                    @Override
                    public void onResponse(Call<Client> call, retrofit2.Response<Client> response) {
                        dismissProgresBar();
                        if (response.code() == 200) {
                            //get the new client Information and save them into our sharedPref
                            ClientController.setClient(response.body());
                            //redirecting to the Account page
                            Fragment selectedFragment = new AccountBFragment();
                            FragmentManager fragmentManager2 = getFragmentManager();
                            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                            fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
                        } else
                            Toast.makeText(getContext(), "ERREUR" + response.code(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<Client> call, Throwable t) {
                        dismissProgresBar();
                        Toast.makeText(getContext(), "Update Failure", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void initializeViews(View view) {
        //Load the buttons
        btnUpdateProfile = view.findViewById(R.id.btnUpdateProfile);
        //Load Other stuff
        imgCustomer_accountFragUpdate = view.findViewById(R.id.imgCustomer_accountFragUpdate);
        EdtTxt_Name = view.findViewById(R.id.EdtTxt_Name);
        EdtTxt_LastName = view.findViewById(R.id.EdtTxt_Lastname);
        EdtTxt_Email = view.findViewById(R.id.EdtTxt_Email);
        EdtTxt_Phone = view.findViewById(R.id.EdtTxt_Phone);
    }

    private void initializeData() {
        client = ClientController.getClient();
    }

    private void setClientInformation() {
        EdtTxt_Name.setText(client.getName());
        EdtTxt_LastName.setText(client.getLastname());
        EdtTxt_Email.setText(client.getEmail());
        EdtTxt_Phone.setText(client.getPhone());
    }
    private void makeProgresBar(){
        progress = new ProgressDialog(getContext());
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar(){progress.dismiss();}

    private void showProgresBar(){progress.show();}
}
