package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.test.Adapter.AlertsRecyclerViewAdapter;
import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Alert;
import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.AlertConsultationActivity;
import com.example.administrator.test.View.MenuGeneral;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeBFragment extends Fragment {
    Alerts alerts;
    Client client;
    TextView numberAlertNotSeen, textToColor;
    private ImageView imageAlert;
    private ProgressDialog progress;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    LinearLayout centerOrSomething;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_home, container, false);
        makeProgresBar();
        initializeViews(view);
        changeTextHeader();
        initializeData();
        loadData();
        return view;
    }

    private void setNumberAlertNotSeen() {
        numberAlertNotSeen.setText(alerts.getAlerts().size());
    }

    private void initializeData() {
        client = ClientController.getClient();
    }
    public void changeTextHeader(){
        if (getActivity() instanceof MenuGeneral)
            ((MenuGeneral) getActivity()).setHeaderText("Home");
        if (getActivity() instanceof AlertConsultationActivity)
            ((AlertConsultationActivity) getActivity()).setHeaderText("Home");
    }
    private void setTheDataListValues() {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new AlertsRecyclerViewAdapter(getActivity(), alerts.getAlerts());
        mRecyclerView.setAdapter(mAdapter);

        Log.i("alerts", "nbr alert : " + alerts.getAlerts().size());
        setText("" + alerts.getAlerts().size());
    }

    private void initializeViews(View view) {
        mRecyclerView = view.findViewById(R.id.my_not_seen_alerts_list_recycler_view);
        numberAlertNotSeen = view.findViewById(R.id.numberAlertNotSeen);
        imageAlert = view.findViewById(R.id.imageAlert);
        textToColor = view.findViewById(R.id.textToColorHome);
        centerOrSomething = view.findViewById(R.id.centerOrSomething);
    }

    private void loadData() {
        showProgresBar();
        ServiceClient.instance().consulterAlertsNotSeen(client.getId()).enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    alerts = response.body();
                    if (alerts.getAlerts().size() > 0) {
                        setColoringBackgroundPict();
                        setTheDataListValues();
                    } else
                        setGrisBackgroundPict();
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });
    }

    private void setColoringBackgroundPict() {
        imageAlert.setBackgroundResource(R.drawable.alertnonvide);
        textToColor.setTextColor(this.getResources().getColor(R.color.orange));
        setTextG("You have notifications pending");
    }

    private void setGrisBackgroundPict() {
        imageAlert.setBackgroundResource(R.drawable.alertvide);
        textToColor.setTextColor(this.getResources().getColor(R.color.dark_blue));
        setTextG("You don't have notifications pending");
        centerOrSomething.setGravity(Gravity.CENTER);
    }

    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }

    public void setTextG(String textMessage) {
        TextView textToColorGris = (TextView) view.findViewById(R.id.textToColorHome);
        textToColorGris.setText(textMessage);
    }

    public void setText(String text) {
        TextView textView = (TextView) getView().findViewById(R.id.numberAlertNotSeen);
        textView.setText(text);
    }

}
