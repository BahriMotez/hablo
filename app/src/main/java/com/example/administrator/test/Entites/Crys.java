package com.example.administrator.test.Entites;

import java.io.Serializable;
import java.util.ArrayList;

public class Crys implements Serializable{

    private ArrayList<Cry> crys;

    public Crys(ArrayList<Cry> crys) {
        this.crys = crys;
    }

    public ArrayList<Cry> getCrys() {
        return crys;
    }

    public void setCrys(ArrayList<Cry> crys) {
        this.crys = crys;
    }

    @Override
    public String toString() {
        return "Crys{" +
                "crys=" + crys +
                '}';
    }
}
