package com.example.administrator.test.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Controller.DeviceController;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Entites.Device;
import com.example.administrator.test.Fragments.AccountBFragment;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationDeviceActivity extends AppCompatActivity {
    Button activate;
    TextView txtView_DeviceID,textHeader;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceController.getDevice() != null) {
            startMainActivity();
        }

        setContentView(R.layout.activity_activation_device);
        makeProgresBar();
        initializeViews();
        actionsButtons();

    }

    private void actionsButtons() {
        activate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ButtonAction();
            }

        });
    }

    private void ButtonAction() {
        showProgresBar();
        ServiceClient.instance().activateDevice(txtView_DeviceID.getText().toString()).enqueue(new Callback<Device>() {
            @Override
            public void onResponse(Call<Device> call, Response<Device> response) {
                if (response.code() == 200) {
                    dismissProgresBar();
                    DeviceController.setDevice(response.body());
                    startMainActivity();
                } else {
                    dismissProgresBar();
                    Toast.makeText(ActivationDeviceActivity.this, "Couldn't activate ur device try later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Device> call, Throwable t) {
                dismissProgresBar();
            }
        });
    }

    private void initializeViews() {
        activate = findViewById(R.id.btnActivateDevice);
        txtView_DeviceID = findViewById(R.id.deviceID_Activation_activity);

    }


//    private boolean isFirstTimeStartApp() {
//        SharedPreferences ref = getApplicationContext().getSharedPreferences("activationDeviceApp", Context.MODE_PRIVATE);
//        return ref.getBoolean("FirstTimeActivate", true);
//    }
//
//    private void setFirstTimeStartStatus(boolean stt) {
//        SharedPreferences ref = getApplicationContext().getSharedPreferences("activationDeviceApp", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = ref.edit();
//        editor.putBoolean("FirstTimeActivate", stt);
//        editor.commit();
//    }

    private void startMainActivity() {
        startActivity(new Intent(ActivationDeviceActivity.this, MainActivity.class));
        finish();
    }

    private void makeProgresBar() {
        progress = new ProgressDialog(ActivationDeviceActivity.this);
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }
}