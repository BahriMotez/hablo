package com.example.administrator.test.Services;

import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Entites.Crys;
import com.example.administrator.test.Entites.Device;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServiceApi {

    @FormUrlEncoded
    @POST("login")
    Call<Client> login(@Field("login") String login,
                       @Field("pwd") String pwd);

    @FormUrlEncoded
    @POST("{id}/Save")
    Call<Child> createAccount(@Path("id") String id,
                              @Field("birthDate") String birthDate,
                              @Field("gender") String gender);

    @FormUrlEncoded
    @POST("{id}/updateinfocustomer")
    Call<Client> updateClient(@Path("id") String id,
                              @Field("name") String name,
                              @Field("lastname") String lastname,
                              @Field("email") String email,
                              @Field("phone") String phone);

    @FormUrlEncoded
    @POST("{id}/passwordupdate")
    Call<Client> updatePasswordClient(@Path("id") String id,
                                      @Field("login") String login,
                                      @Field("oldpwd") String oldpwd,
                                      @Field("newpwd") String newpwd);

    @FormUrlEncoded
    @POST("activatedevice")
    Call<Device> activateDevice(@Field("token") String token);

    @GET("{id}/bebe")
    Call<Childs> consulterBebe(@Path("id") String id);

    @GET("{id}/alertnotseen")
    Call<Alerts> consulterAlertsNotSeen(@Path("id") String id);

    @GET("{id}/alert")
    Call<Alerts> consulterAlerts(@Path("id") String id);

    @GET("cryalert/{idCustomer}/{idAlert}")
    Call<Crys> consulterListCryFilesPaths(@Path("idCustomer") String idCustomer,
                                          @Path("idAlert") String idAlert);

    @GET("alertsbytype/{idCustomer}/{idAlert}")
    Call<Alerts> getAlertsByType(@Path("idCustomer") String idCustomer,
                               @Path("idAlert") String idAlert);

    @GET("{idCustomer}/alert/{idAlert}")
    Call<Alerts> setAlertAsSeen(@Path("idCustomer") String idCustomer,
                                @Path("idAlert") String idAlert);


    @FormUrlEncoded
    @POST("setdevicetoken/{id}")
    Call<Client> setDeviceToken(@Path("id") String id,
                                @Field("tokenDevice") String token);
}
