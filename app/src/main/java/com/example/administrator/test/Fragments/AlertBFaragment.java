package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Entites.Cry;
import com.example.administrator.test.Entites.Crys;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.AlertConsultationActivity;
import com.example.administrator.test.View.MenuGeneral;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class AlertBFaragment extends Fragment {
    public Boolean isMute = false;
    public Boolean isPlaying = false;
    View view;
    private Handler mHandler = new Handler();
    Client client = null;
    ProgressDialog progress;
    String titleAlert, messageAlt, idAlert;
    Cry cry;
    ImageButton stopSound, playSound;
    MediaPlayer mediaPlayer;
    SeekBar playSoundFile;
    Runnable mRunnable;
    private TextView mDuration;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_alert_consultation, container, false);
        client = ClientController.getClient();
        initializeViews(view);
        changeTextHeader();
        makeProgresBar();
        Intent intent_o = getActivity().getIntent();
        if (intent_o.getStringExtra("clickAction").equals("yes")) {
            titleAlert = intent_o.getStringExtra("title");
            messageAlt = intent_o.getStringExtra("message");
            idAlert = intent_o.getStringExtra("idalert");

        } else {
            titleAlert = intent_o.getStringExtra("title");
            messageAlt = intent_o.getStringExtra("message");
            idAlert = intent_o.getStringExtra("idalert");
        }
        setAlertAsSeen(idAlert);

        //get the data from database
        getTheCryFilePathFromDataBase(idAlert);

        setText(messageAlt);

        return view;
    }

    private void getTheCryFilePathFromDataBase(String idAlert) {
        showProgresBar();
        ServiceClient.instance().consulterListCryFilesPaths(client.getId(), idAlert).enqueue(new Callback<Crys>() {
            @Override
            public void onResponse(Call<Crys> call, retrofit2.Response<Crys> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    cry = response.body().getCrys().get(0);
                    action_buttons();
                }
            }

            @Override
            public void onFailure(Call<Crys> call, Throwable t) {
                dismissProgresBar();
            }
        });
    }
    public void changeTextHeader(){
        if (getActivity() instanceof MenuGeneral)
            ((MenuGeneral) getActivity()).setHeaderText("Alerts");
        if (getActivity() instanceof AlertConsultationActivity)
            ((AlertConsultationActivity) getActivity()).setHeaderText("Alerts");
    }
    private void initializemediaPlayer() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(cry.getSrcAudioFile().toString());

            mediaPlayer.prepare();
            playSoundFile.setMax(mediaPlayer.getDuration());
            initializeSeekBar();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void action_buttons() {
        initializemediaPlayer();
        stopSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPlaying){
                    stopSound.setBackgroundResource(R.drawable.play);
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        mHandler.removeCallbacks(mRunnable);
                    }
                    isPlaying = false;
                }
                else{
                    stopSound.setBackgroundResource(R.drawable.pause);
                    if (!mediaPlayer.isPlaying()) {
                        mediaPlayer.start();
                        mHandler.postDelayed(mRunnable, 0);
                    }
                    isPlaying = true;
                }
            }
        });
    }

    protected void initializeSeekBar() {
        playSoundFile.setMax(mediaPlayer.getDuration() / 1000);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000; // In milliseconds
                    playSoundFile.setProgress(mCurrentPosition);
                    getAudioStats();
                }
                mHandler.postDelayed(mRunnable, 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 1000);
    }

    protected void getAudioStats() {
        int duration = mediaPlayer.getDuration() / 1000; // In milliseconds
        int due = (mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition()) / 1000;
        int pass = duration - due;
        int minut = 0;
        int sec = 0;
        minut = (int) pass / 60;
        sec = pass - (minut * 60);
        if(sec<10)
            setMDuration(minut + ":0" + sec);
        else
            setMDuration(minut + ":" + sec);
    }

    private void initializeViews(View view) {
        stopSound = view.findViewById(R.id.stopSound);
        stopSound.setBackgroundResource(R.drawable.play);
        playSoundFile = view.findViewById(R.id.playSoundFile);
    }

    private void setAlertAsSeen(String idAlert) {
        showProgresBar();
        ServiceClient.instance().setAlertAsSeen(client.getId(), idAlert).enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, retrofit2.Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                Toast.makeText(getContext(), "Failure", Toast.LENGTH_LONG).show();
                dismissProgresBar();
            }
        });
    }

    public void setText(String textMessage) {
        TextView texMessageAlert = (TextView) view.findViewById(R.id.description_alert);
        texMessageAlert.setText(textMessage);
    }

    public void setMDuration(String duree) {
        TextView textduree = (TextView) view.findViewById(R.id.mDuration);
        textduree.setText(duree);
    }

    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        mHandler.removeCallbacks(mRunnable);
    }
}
