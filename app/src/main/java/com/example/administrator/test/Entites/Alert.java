package com.example.administrator.test.Entites;

public class Alert {

    private Integer id;
    private Integer customerId;
    private String title;
    private String alertDate;
    private String description;
    private Integer seen;

    public Alert(Integer id, Integer customerId, String alertDate, String description, Integer seen,String title) {
        this.id = id;
        this.customerId = customerId;
        this.alertDate = alertDate;
        this.description = description;
        this.seen = seen;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getAlertDate() {
        return alertDate;
    }

    public void setAlertDate(String alertDate) {
        this.alertDate = alertDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSeen() {
        return seen;
    }

    public void setSeen(Integer seen) {
        this.seen = seen;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", alertDate='" + alertDate + '\'' +
                ", description='" + description + '\'' +
                ", seen=" + seen +
                '}';
    }
}
