package com.example.administrator.test.Entites;

import java.io.Serializable;

public class Child implements Serializable{

    private String birthDate;
    private String gender;
    private String id;
    private String customer_id;
    private String inscriDate;

    public Child(String birthDate, String gender, String id, String customer_id, String inscriDate) {
        this.birthDate = birthDate;
        this.gender = gender;
        this.id = id;
        this.customer_id = customer_id;
        this.inscriDate = inscriDate;
    }

    public Child(String birthDate, String gender, String customer_id) {
        this.birthDate = birthDate;
        this.customer_id = customer_id;
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getInscriDate() {
        return inscriDate;
    }

    public void setInscriDate(String inscriDate) {
        this.inscriDate = inscriDate;
    }

    @Override
    public String toString() {
        return "Child{" +
                "birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", id='" + id + '\'' +
                ", customer_id='" + customer_id + '\'' +
                ", inscriDate='" + inscriDate + '\'' +
                '}';
    }
}

