package com.example.administrator.test.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.administrator.test.R;

public class UpdateDeviceFragment extends Fragment {
    WebView webView_inside;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_update_device,container,false);
        intializeViews(view);
        loadWebView();

        return view;
    }

    private void intializeViews(View view) {
        webView_inside = view.findViewById(R.id.webView_inside);
    }

    private void loadWebView() {
        webView_inside.loadUrl("http://10.54.234.219");

        // Enable Javascript
        WebSettings webSettings = webView_inside.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        webView_inside.setWebViewClient(new WebViewClient());
    }
}
