package com.example.administrator.test.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.R;
import com.example.administrator.test.View.MainActivity;
import com.example.administrator.test.View.MenuGeneral;

public class LogOutFragment extends Fragment {
    ImageButton buttonLogout;
    ImageButton buttonCancel;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logout,container,false);
        buttonLogout = view.findViewById(R.id.buttonLogout);
        buttonCancel = view.findViewById(R.id.buttonCancel);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Logout();
                startMainActivity();
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startMenuActivity();
            }
        });
        return view;
    }
    public void Logout(){
        ClientController.destroyClient();
    }
    private void startMainActivity() {
        startActivity(new Intent(getContext(), MainActivity.class));
    }

    private void startMenuActivity(){
        startActivity(new Intent(getContext(), MenuGeneral.class));
    }
}
