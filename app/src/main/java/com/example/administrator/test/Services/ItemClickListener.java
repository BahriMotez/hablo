package com.example.administrator.test.Services;

import android.view.View;

public interface ItemClickListener {

    void onItemClick(View v,int pos);
}
