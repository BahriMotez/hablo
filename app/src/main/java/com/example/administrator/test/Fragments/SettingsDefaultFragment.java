package com.example.administrator.test.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.View.ActivationDeviceActivity;
import com.example.administrator.test.View.AlertConsultationActivity;
import com.example.administrator.test.View.MainActivity;
import com.example.administrator.test.View.MenuGeneral;
import com.squareup.picasso.Picasso;

public class SettingsDefaultFragment extends Fragment {

    ImageButton btnUpdateProfileSettingsAct, btnAboutUsSettingsActivity, btnLogout,btnUpdateDevice;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_default_page, container, false);
        changeTextHeader();
        initializeViews(view);
        actionsButtons();

        return view;
    }

    public void changeTextHeader(){
        if (getActivity() instanceof MenuGeneral)
            ((MenuGeneral) getActivity()).setHeaderText("Settings");
        if (getActivity() instanceof AlertConsultationActivity)
            ((AlertConsultationActivity) getActivity()).setHeaderText("Settings");
    }

    private void actionsButtons() {
        btnUpdateProfileSettingsAct.setOnClickListener(new View.OnClickListener() {
                                                           public void onClick(View v) {
                                                               Fragment selectedFragment = new AccountBFragment();
                                                               FragmentManager fragmentManager2 = getFragmentManager();
                                                               FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                                                               fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
                                                           }
                                                       }
        );
        btnAboutUsSettingsActivity.setOnClickListener(new View.OnClickListener() {
                                                          public void onClick(View v) {
                                                              Fragment selectedFragment = new AboutUsFragment();
                                                              FragmentManager fragmentManager2 = getFragmentManager();
                                                              FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                                                              fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
                                                          }
                                                      }
        );
        btnLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new LogOutFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
        btnUpdateDevice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new UpdateDeviceFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
    }

    public void initializeViews(View view) {
        btnUpdateProfileSettingsAct = view.findViewById(R.id.btnUpdateProfileSettingsAct);
        btnAboutUsSettingsActivity = view.findViewById(R.id.btnAboutUsSettingsActivity);
        btnLogout = view.findViewById(R.id.btnLogout);
        btnUpdateDevice= view.findViewById(R.id.btnUpdateDevice);
    }
}
