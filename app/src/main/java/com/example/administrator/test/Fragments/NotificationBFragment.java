package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.test.Adapter.AlertsRecyclerViewAdapter;
import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationBFragment extends Fragment {
    Alerts alerts;
    Client client;
    private ProgressDialog progress;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_notification,container,false);
        makeProgresBar();
        initializeViews(view);
        initializeData();
        loadData();
        return view;
    }

    private void initializeData() {
        client = ClientController.getClient();
    }

    private void setTheDataListValues() {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new AlertsRecyclerViewAdapter(getActivity(),alerts.getAlerts());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initializeViews(View view) {
        mRecyclerView = view.findViewById(R.id.all_alerts_list_recycler_view);
    }
    private void loadData() {
        showProgresBar();
        ServiceClient.instance().consulterAlerts(client.getId()).enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    alerts = response.body();
                    setTheDataListValues();
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });
    }
    private void makeProgresBar(){
        progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar(){progress.dismiss();}

    private void showProgresBar(){progress.show();}
}
