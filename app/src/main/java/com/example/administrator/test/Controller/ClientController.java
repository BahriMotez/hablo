package com.example.administrator.test.Controller;

import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Utils.SharedPrefsManager;
import com.google.gson.Gson;

public class ClientController {

    private static final String KEY_CLIENT = "key_client";

    public static void setClient(Client value) {
        String json = new Gson().toJson(value);
        SharedPrefsManager.instance.edit().putString(KEY_CLIENT, json).apply();
    }

    public static Client getClient() {
        String str = SharedPrefsManager.instance.getString(KEY_CLIENT, null);
        return new Gson().fromJson(str, Client.class);
    }

    public static void destroyClient() {
        SharedPrefsManager.instance.edit().remove(KEY_CLIENT).apply();
    }
}
