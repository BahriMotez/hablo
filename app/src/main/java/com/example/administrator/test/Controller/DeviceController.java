package com.example.administrator.test.Controller;

import com.example.administrator.test.Entites.Device;
import com.example.administrator.test.Utils.SharedPrefsManager;
import com.google.gson.Gson;

public class DeviceController {

    private static final String KEY_DEVICE = "key_device";

    public static void setDevice(Device value) {
        String json = new Gson().toJson(value);
        SharedPrefsManager.instance.edit().putString(KEY_DEVICE, json).apply();
    }

    public static Device getDevice() {
        String str = SharedPrefsManager.instance.getString(KEY_DEVICE, null);
        return new Gson().fromJson(str, Device.class);
    }
}
