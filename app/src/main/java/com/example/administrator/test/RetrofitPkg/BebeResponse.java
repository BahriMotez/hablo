package com.example.administrator.test.RetrofitPkg;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


import com.example.administrator.test.Controller.ChildsController;

import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Services.ServiceBebe;
import com.example.administrator.test.View.*;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BebeResponse {
    Context ctx;
    String url;


    public BebeResponse(Context ctx, String url) {
        this.ctx = ctx;
        this.url = url;
    }

    public void sendNetworkRequest(Child child) {

        RequestBody birthdate = RequestBody.create(MediaType.parse("text/plain"), child.getBirthDate());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), child.getGender());


        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        ServiceBebe srv = retrofit.create(ServiceBebe.class);
        Call<Child> call = srv.createAccount(birthdate, gender);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, retrofit2.Response<Child> response) {
                Toast.makeText(ctx, "yes" + response.code(), Toast.LENGTH_LONG).show();

                if (response.code() == 200) {
                    Intent myIntent = new Intent(ctx, MenuGeneral.class);
                    ctx.startActivity(myIntent);
                } else
                    Toast.makeText(ctx, "ERREUR" + response.code(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                Toast.makeText(ctx, "Failure", Toast.LENGTH_LONG).show();
                //  Log.d("Login", t.getLocalizedMessage());
            }
        });

    }
}
