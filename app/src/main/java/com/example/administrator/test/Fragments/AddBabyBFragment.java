package com.example.administrator.test.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.RetrofitPkg.BebeResponse;
import com.example.administrator.test.Services.ServiceBebe;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.MenuGeneral;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddBabyBFragment extends Fragment {
    ImageButton btnAddBaby, pickBirthdateButton, pickGenderButton;
    Client client = null;
    ProgressDialog progress;
    View view;
    public String gender = "" ;
    public static String dateBirth = "";
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    int year,month,day;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_baby_account, container, false);
        makeProgresBar();
        initializeData();
        initializeView(view);
        actionButtton();
        return view;
    }

    private void actionButtton() {
        btnAddBaby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dateBirth != "" && gender != "")
                    sendNetworkRequest(new Child(dateBirth, gender, client.getId()));
                else
                    Toast.makeText(getContext(), "Pick gender of ur baby and his bithdate", Toast.LENGTH_LONG).show();
            }
        });
        pickBirthdateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void initializeView(View view) {
        btnAddBaby = view.findViewById(R.id.btnAddBaby);
        pickBirthdateButton = view.findViewById(R.id.pickBirthdateButton);
        pickGenderButton = view.findViewById(R.id.pickGenderButton);

        registrationForContextMenu();
    }

    private void registrationForContextMenu() {
        this.registerForContextMenu(pickGenderButton);
        this.registerForContextMenu(pickBirthdateButton);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    private void initializeData() {
        client = ClientController.getClient();
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                dateBirth = year +"-"+monthOfYear+"-"+dayOfMonth;
                Log.i("birthdate",dateBirth);
            }

        };

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.pickGenderButton) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.gender_context_menu, menu);
        }
        if (v.getId() == R.id.pickBirthdateButton) {
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.genderBoyCheckbox:
                gender = "1";
                return true;
            case R.id.genderGirlCheckbox:
                gender = "0";
                return true;
        }
        return super.onContextItemSelected(item);
    }

    public void sendNetworkRequest(Child child) {
        showProgresBar();
        ServiceClient.instance().createAccount(client.getId(), child.getBirthDate(), child.getGender()).enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, retrofit2.Response<Child> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    Fragment selectedFragment = new AccountBFragment();
                    FragmentManager fragmentManager2 = getFragmentManager();
                    FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                    fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
                } else
                    Toast.makeText(getContext(), "ERREUR" + response.code(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                Toast.makeText(getContext(), "Failure", Toast.LENGTH_LONG).show();
                dismissProgresBar();
            }
        });
    }

    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }
}
