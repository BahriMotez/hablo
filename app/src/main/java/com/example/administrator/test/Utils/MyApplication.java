package com.example.administrator.test.Utils;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.Utils.SharedPrefsManager;

public class MyApplication extends Application {
    public static final String CHANNEL_ID = "hablo_channel1";

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPrefsManager.initialize(getApplicationContext());
        createNotificationChannel();
    }
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel_notif = new NotificationChannel(
                    CHANNEL_ID,
                    "channel_1_hablo",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel_notif.setDescription("this is hablo channel notification");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel_notif);
        }
    }

}
