package com.example.administrator.test.Controller;

import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Utils.SharedPrefsManager;
import com.google.gson.Gson;

public class ChildsController {

    private static final String KEY_CHILDS = "key_childs";

    public static void setChilds(Childs value) {
        String json = new Gson().toJson(value);
        SharedPrefsManager.instance.edit().putString(KEY_CHILDS, json).apply();
    }

    public static Childs getChilds() {
        String str = SharedPrefsManager.instance.getString(KEY_CHILDS, null);
        return new Gson().fromJson(str, Childs.class);
    }
}
