package com.example.administrator.test.Entites;

import java.util.ArrayList;

public class Alerts {
    private ArrayList<Alert> alerts;

    public Alerts(ArrayList<Alert> alerts) {
        this.alerts = alerts;
    }

    public ArrayList<Alert> getAlerts() {
        return alerts;
    }

    public void setAlerts(ArrayList<Alert> alerts) {
        this.alerts = alerts;
    }

    @Override
    public String toString() {
        return "Alerts{" +
                "alerts=" + alerts +
                '}';
    }
}
