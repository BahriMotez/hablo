package com.example.administrator.test.Entites;

import com.example.administrator.test.Entites.Child;

import java.io.Serializable;
import java.util.ArrayList;

public class Childs implements Serializable {

    private int nbBebes;
    private ArrayList<Child> childs;

    public Childs(int nbBebes, ArrayList<Child> tab) {
        this.nbBebes = nbBebes;
        this.childs = tab;
    }

    public int getNbBebes() {
        return nbBebes;
    }

    public void setNbBebes(int nbBebes) {
        this.nbBebes = nbBebes;
    }

    public ArrayList<Child> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<Child> tab) {
        this.childs = tab;
    }

    @Override
    public String toString() {
        return "Childs{" +
                "nbBebes=" + nbBebes +
                ", childs=" + childs +
                '}';
    }
}
