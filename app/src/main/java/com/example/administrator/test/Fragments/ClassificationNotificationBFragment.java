package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Alerts;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.example.administrator.test.View.AlertConsultationActivity;
import com.example.administrator.test.View.MenuGeneral;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassificationNotificationBFragment  extends Fragment {
    ImageButton btnHygiene,btnSleep,btnHunger,btnMood;
    TextView textHeader;
    ProgressDialog progress;
    Client client = null;
    int nbrSleepNotif = 0,nbrHungerNotif = 0,nbrHygieneNotif = 0,nbrMoodNotif = 0;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_notification_classification,container,false);
        initializeViews(view);
        client = ClientController.getClient();
        makeProgresBar();
        getAlertNumbers();
        changeTextHeader();
        actionButtons();
        return view;
    }
    private void getAlertNumbers(){
        showProgresBar();
        ServiceClient.instance().getAlertsByType(client.getId(), "1").enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    Alerts alerts = response.body();
                    if(alerts.getAlerts().size() < 10)
                        setnbrHungerNotif("0"+alerts.getAlerts().size()+"");
                    else
                        setnbrHungerNotif(alerts.getAlerts().size()+"");
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });
        ServiceClient.instance().getAlertsByType(client.getId(), "2").enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    Alerts alerts = response.body();
                    if(alerts.getAlerts().size() < 10)
                        setnbrMoodNotif("0"+alerts.getAlerts().size());
                    else
                        setnbrMoodNotif(alerts.getAlerts().size()+"");
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });
        ServiceClient.instance().getAlertsByType(client.getId(), "3").enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    Alerts alerts = response.body();
                    if(alerts.getAlerts().size() < 10)
                        setnbrSleepNotif("0"+alerts.getAlerts().size());
                    else
                        setnbrSleepNotif(alerts.getAlerts().size()+"");
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });
        ServiceClient.instance().getAlertsByType(client.getId(), "4").enqueue(new Callback<Alerts>() {
            @Override
            public void onResponse(Call<Alerts> call, Response<Alerts> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    Alerts alerts = response.body();
                    if(alerts.getAlerts().size() < 10)
                        setnbrHygieneNotif("0"+alerts.getAlerts().size());
                    else
                        setnbrHygieneNotif(alerts.getAlerts().size()+"");
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Alerts> call, Throwable t) {
                dismissProgresBar();
            }
        });

    }
    private void changeTextHeader(){
        if (getActivity() instanceof MenuGeneral)
            ((MenuGeneral) getActivity()).setHeaderText("Alerts");
        if (getActivity() instanceof AlertConsultationActivity)
            ((AlertConsultationActivity) getActivity()).setHeaderText("Alerts");
    }
    private void actionButtons() {
        btnHygiene.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new HygieneBFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
        btnSleep.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new SleepBFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
        btnHunger.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new HungerFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
        btnMood.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Fragment selectedFragment = new MoodBFragment();
                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
            }
        });
    }

    private void initializeViews(View view) {
        btnHygiene = view.findViewById(R.id.btnHygiene);
        btnSleep = view.findViewById(R.id.btnSleep);
        btnHunger = view.findViewById(R.id.btnHunger);
        btnMood = view.findViewById(R.id.btnMood);;
    }
    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Verifying");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }

    public void setnbrHygieneNotif(String textMessage) {
        TextView nbrHygieneNotif = (TextView) view.findViewById(R.id.nbrHygieneNotif);
        nbrHygieneNotif.setText(textMessage);
    }
    public void setnbrSleepNotif(String textMessage) {
        TextView nbrSleepNotif = (TextView) view.findViewById(R.id.nbrSleepNotif);
        nbrSleepNotif.setText(textMessage);
    }
    public void setnbrHungerNotif(String textMessage) {
        TextView nbrHungerNotif = (TextView) view.findViewById(R.id.nbrHungerNotif);
        nbrHungerNotif.setText(textMessage);
    }
    public void setnbrMoodNotif(String textMessage) {
        TextView nbrMoodNotif = (TextView) view.findViewById(R.id.nbrMoodNotif);
        nbrMoodNotif.setText(textMessage);
    }
}