package com.example.administrator.test.View;

import android.Manifest;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;

import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.RetrofitPkg.BebeResponse;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ajouterCaracBebe extends AppCompatActivity {
    private static final int READ_STORAGE_PERMISSION_REQUEST_CODE = 10;
    BebeResponse bResponse ;
    EditText poids ;
    EditText name ;
    EditText lastname ;
    EditText birthDate ;
    RadioButton garcon;
    RadioButton fille;
    Client client;
    Button btn;
    ImageButton img ;
    private String url;
    private final int IMG_REQUEST = 1 ;
    private Bitmap bitmap;
    String imagePath;
    Uri path;
    private String selectedImagePath;

    private String getRealPath (Uri uri)
    {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(),uri,proj,null,null,null);
        Cursor cursor =loader.loadInBackground();
        int c = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String r = cursor.getString(c);
        cursor.close();
        return r;

    }

    String getAbsolutePath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMG_REQUEST && resultCode==RESULT_OK && data!=null)
        {
            path = data.getData();
            try
            {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                img.setImageBitmap(bitmap);

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            Uri originalUri = data.getData();

            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.
            getContentResolver().takePersistableUriPermission(originalUri, takeFlags);

    /* now extract ID from Uri path using getLastPathSegment() and then split with ":"
    then call get Uri to for Internal storage or External storage for media I have used getUri()
    */

            String id = originalUri.getLastPathSegment().split(":")[1];
            final String[] imageColumns = {MediaStore.Images.Media.DATA };
            final String imageOrderBy = null;

            Uri uri = getUri();
            selectedImagePath = "path";

            Cursor imageCursor = managedQuery(uri, imageColumns,
                    MediaStore.Images.Media._ID + "="+id, null, imageOrderBy);

            if (imageCursor.moveToFirst()) {
                selectedImagePath = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            Log.e("path",selectedImagePath );
        }
    }

    public boolean checkPermissionForReadExtertalStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    public void requestPermissionForReadExtertalStorage() throws Exception {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_STORAGE_PERMISSION_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // By using this method get the Uri of Internal/External Storage for Media
    private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if(!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_ajouter_carac_bebe);
//        client = ClientController.getClient();
//        url = "http://10.68.11.61:5000/restApi/Hablo_v1/Android/"+client.getId()+"/";
//
//        btn = (Button) findViewById(R.id.button4);
//        name = (EditText) findViewById(R.id.editText4);
//        lastname = (EditText) findViewById(R.id.editText3);
//        birthDate = (EditText) findViewById(R.id.editText5);
//        poids = (EditText) findViewById(R.id.editText7);
//        garcon=(RadioButton)  findViewById(R.id.radioButton2);
//        fille=(RadioButton)  findViewById(R.id.radioButton3);
//        img = (ImageButton) findViewById(R.id.imageButton);


        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



                Intent intent;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                }else{
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                }
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                startActivityForResult(intent, IMG_REQUEST);

            }

        });


        if (!checkPermissionForReadExtertalStorage()) {
            try {
                requestPermissionForReadExtertalStorage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String nameB = name.getText().toString();
                final String lastnameB = lastname.getText().toString();
                final String birthDateB = birthDate.getText().toString();
                final String poidsB= poids.getText().toString();
                final String genreB ;
                if (garcon.isChecked())
                    genreB="1";
                else genreB="0";
                Log.d("Login", path+"");
                imagePath = getRealPath(path);

                File f = new File (selectedImagePath);
                RequestBody r = RequestBody.create(MediaType.parse("multipart/form-data"),f);
                MultipartBody.Part p = MultipartBody.Part.createFormData("srcImg",f.getName(),r);




                //Child b = new Child (birthDateB,genreB,lastnameB,nameB,poidsB);
                bResponse = new BebeResponse(ajouterCaracBebe.this,url);
                //bResponse.sendNetworkRequest(b);



            }

        });


    }
}
