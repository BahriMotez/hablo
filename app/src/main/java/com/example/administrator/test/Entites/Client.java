package com.example.administrator.test.Entites;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Client implements Serializable {
    private String id;
    private String lastname;
    private String name;
    private String active;
    private String adresse;
    private String altImg;
    private String birthDate;
    private String email;
    private String inscripDate;
    private String login;
    private String phone;
    private String pwd;
    private String srcImg;

    public Client(String login, String pwd) {
        this.login = login;
        this.pwd = pwd;
    }

    public Client(Client client) {
        this.id = client.id;
        this.lastname = client.lastname;
        this.name = client.name;
        this.active = client.active;
        this.adresse = client.adresse;
        this.altImg = client.altImg;
        this.birthDate = client.birthDate;
        this.email = client.email;
        this.inscripDate = client.inscripDate;
        this.login = client.login;
        this.phone = client.phone;
        this.pwd = client.pwd;
        this.srcImg = client.srcImg;
    }

    public Client(String id, String lastname, String name, String active, String adresse, String altImg, String birthDate, String email, String inscripDate, String login, String phone, String pwd, String srcImg) {
        this.id = id;
        this.lastname = lastname;
        this.name = name;
        this.active = active;
        this.adresse = adresse;
        this.altImg = altImg;
        this.birthDate = birthDate;
        this.email = email;
        this.inscripDate = inscripDate;
        this.login = login;
        this.phone = phone;
        this.pwd = pwd;
        this.srcImg = srcImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAltImg() {
        return altImg;
    }

    public void setAltImg(String altImg) {
        this.altImg = altImg;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInscripDate() {
        return inscripDate;
    }

    public void setInscripDate(String inscripDate) {
        this.inscripDate = inscripDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSrcImg() {
        return srcImg;
    }

    public void setSrcImg(String srcImg) {
        this.srcImg = srcImg;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", name='" + name + '\'' +
                ", active='" + active + '\'' +
                ", adresse='" + adresse + '\'' +
                ", altImg='" + altImg + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", email='" + email + '\'' +
                ", inscripDate='" + inscripDate + '\'' +
                ", login='" + login + '\'' +
                ", phone='" + phone + '\'' +
                ", pwd='" + pwd + '\'' +
                ", srcImg='" + srcImg + '\'' +
                '}';
    }
}
