package com.example.administrator.test.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.administrator.test.Entites.Client;
import com.google.gson.Gson;

public class SharedPrefsManager {

    public static SharedPreferences instance;

    public static void initialize(Context context) {
        instance = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    }
}
