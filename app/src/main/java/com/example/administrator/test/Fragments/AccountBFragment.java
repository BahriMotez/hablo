package com.example.administrator.test.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.test.Adapter.RecyclerViewAdapter;
import com.example.administrator.test.Controller.ClientController;
import com.example.administrator.test.Entites.Childs;
import com.example.administrator.test.Entites.Client;
import com.example.administrator.test.R;
import com.example.administrator.test.Services.ServiceClient;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountBFragment extends Fragment {
    ImageButton btnAddBaby_Fragmentshow;
    Client client;
    Childs childs;
    private ProgressDialog progress;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_account, container, false);
        makeProgresBar();
        //get the connected client from the Client Controller (SharedPreference)
        initializeData();
        //get the buttons from the view
        initializeViews(view);
        //Load Baby List
        loadData();
        //do the Action of the two button
        actionsButtons();
        return view;
    }
    private void setTheDataListValues() {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerViewAdapter(childs.getChilds());
        mRecyclerView.setAdapter(mAdapter);
    }
    private void loadData() {
        showProgresBar();
        ServiceClient.instance().consulterBebe(client.getId()).enqueue(new Callback<Childs>() {
            @Override
            public void onResponse(Call<Childs> call, Response<Childs> response) {
                dismissProgresBar();
                if (response.code() == 200) {
                    childs = response.body();
                    setTheDataListValues();
                } else {
                    dismissProgresBar();
                }
            }

            @Override
            public void onFailure(Call<Childs> call, Throwable t) {
                dismissProgresBar();
            }
        });
    }

    private void actionsButtons() {
        btnAddBaby_Fragmentshow.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View v) {
                                                    Fragment selectedFragment = new AddBabyBFragment();
                                                    FragmentManager fragmentManager2 = getFragmentManager();
                                                    FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                                                    fragmentTransaction2.replace(R.id.container, selectedFragment).addToBackStack("home").commit();
                                                }
                                            }
        );
    }

    public void initializeViews(View view) {
        mRecyclerView = view.findViewById(R.id.my_baby_list_recycler_view);
        btnAddBaby_Fragmentshow = view.findViewById(R.id.btnAddBaby_Fragmentshow);
    }

    public void initializeData() {
        client = ClientController.getClient();
    }
    private void makeProgresBar() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
    }

    private void dismissProgresBar() {
        progress.dismiss();
    }

    private void showProgresBar() {
        progress.show();
    }
}
