package com.example.administrator.test.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.test.Entites.Child;
import com.example.administrator.test.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ListItemViewHolder> {

    private ArrayList<Child> items;
    private SparseBooleanArray selectedItems;

    public RecyclerViewAdapter(ArrayList<Child> childData) {
        if (childData == null) {
            throw new IllegalArgumentException("Child Data List must not be null");
        }
        items = childData;
        selectedItems = new SparseBooleanArray();
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.custom_layout_show_babies_account, viewGroup, false);
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        Child child = items.get(position);

        viewHolder.birthDate.setText(child.getBirthDate().substring(5,16));
        String genderT;
        if (child.getGender().equals("1")) {
            genderT = "Boy";
            viewHolder.imageGender.setBackgroundResource(R.drawable.boy);
        } else {
            genderT = "Girl";
            viewHolder.imageGender.setBackgroundResource(R.drawable.girl);
        }
        viewHolder.gender.setText(String.valueOf(genderT));
        viewHolder.itemView.setActivated(selectedItems.get(position, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public final static class ListItemViewHolder extends RecyclerView.ViewHolder {
        TextView birthDate;
        TextView gender;
        ImageView imageGender;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            birthDate = itemView.findViewById(R.id.textBirthDate);
            gender = itemView.findViewById(R.id.textGender);
            imageGender = itemView.findViewById(R.id.imageGender);
        }
    }
}
